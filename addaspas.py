import csv

with open('QC/predictionTestQC_MLP.csv', 'r') as csvinput:
    with open('QC/submissionTestQC_MLP.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)

        all = []
        row = next(reader)
        row.clear()
        row.append('"Id"')
        row.append('"Genres"')
        all.append(row)

        for row in reader:
            row_n = int(row[0]) - 1
            row_class = row[2]
            if "Blues" in row_class:
                sampleClass = 1
            elif "Classical" in row_class:
                sampleClass = 2
            elif "Jazz" in row_class:
                sampleClass = 3
            elif "Metal" in row_class:
                sampleClass = 4
            elif "Pop" in row_class:
                sampleClass = 5
            elif "Rock" in row_class:
                sampleClass = 6

            row_1 = '\"' + str(row_n) + '\"'
            row_2 = '\"' + str(sampleClass) + '\"'
            row.clear()
            row.append(row_1)
            row.append(row_2)
            all.append(row)

        writer.writerows(all)
