import csv


def somaArray(array, qtd, inicio):
    index = 0
    soma = 0.0
    while index != qtd:
        soma += float(array[inicio])
        inicio += 1
        index += 1

    return str(soma)

with open('genresTrain.csv', 'r') as csvinput1:
    with open('genresTest.csv', 'r') as csvinput2:
        with open('genresTrainSC.csv', 'w') as csvoutput1:
            with open('genresTestSC.csv', 'w') as csvoutput2:
                writerTrain = csv.writer(csvoutput1, lineterminator='\n')
                writerTest = csv.writer(csvoutput2, lineterminator='\n')
                readerTrain = csv.reader(csvinput1)
                readerTest = csv.reader(csvinput2)

                allTrain = []
                allTest = []
                header = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'P10', 'P11', 'P12', 'P13', 'P14', 'P15', 'P16', 'GENRE']
                rowsTrain = next(readerTrain)
                rowsTest = next(readerTest)
                allTrain.append(header)
                allTest.append(header)

                for rowTrain in readerTrain:
                    newRow = []
                    newRow.append(rowTrain[0])
                    newRow.append(rowTrain[1])
                    newRow.append(rowTrain[2])

                    newRow.append(somaArray(rowTrain, 33,3))
                    newRow.append(rowTrain[37])
                    newRow.append(somaArray(rowTrain, 72-39, 38))
                    newRow.append(rowTrain[72])
                    newRow.append(float(rowTrain[73])+float(rowTrain[74]))
                    newRow.append(float(rowTrain[75])+float(rowTrain[76]))
                    newRow.append(somaArray(rowTrain, 101-78, 77))
                    newRow.append(rowTrain[101])
                    newRow.append(somaArray(rowTrain, 126-103, 102))
                    newRow.append(rowTrain[126])
                    newRow.append(somaArray(rowTrain, 147-128, 127))
                    newRow.append(somaArray(rowTrain, 167-148, 147))
                    newRow.append(somaArray(rowTrain, 191-168, 167))
                    newRow.append(rowTrain[191])
                    allTrain.append(newRow)
# a) parameter 1: Temporal Centroid,
# b) parameter 2: Spectral Centroid average value,
# c) parameter 3: Spectral Centroid variance,
# d) parameters 4-37: Audio Spectrum Envelope (ASE) average values in 34 frequency bands
# e) parameter 38: ASE average value (averaged for all frequency bands)
# f) parameters 39-72: ASE variance values in 34 frequency bands
# g) parameter 73: averaged ASE variance parameters
# h) parameters 74,75: Audio Spectrum Centroid – average and variance values
# i) parameters 76,77: Audio Spectrum Spread – average and variance values
# j) parameters 78-101: Spectral Flatness Measure (SFM) average values for 24 frequency bands
# k) parameter 102: SFM average value (averaged for all frequency bands)
# l) parameters 103-126: Spectral Flatness Measure (SFM) variance values for 24 frequency bands
# m) parameter 127: averaged SFM variance parameters
# n) parameters 128-147: 20 first mel cepstral coefficients average values
# o) parameters 148-167: the same as 128-147
# p) parameters 168-191: dedicated parameters in time domain based of the analysis of the distribution of the envelope in relation to the rms value.

                for rowTest in readerTest:
                    newRow = []
                    newRow.append(rowTest[0])
                    newRow.append(rowTest[1])
                    newRow.append(rowTest[2])

                    newRow.append(somaArray(rowTest, 33,3))
                    newRow.append(rowTest[37])
                    newRow.append(somaArray(rowTest, 72-39, 38))
                    newRow.append(rowTest[72])
                    newRow.append(float(rowTest[73])+float(rowTest[74]))
                    newRow.append(float(rowTest[75])+float(rowTest[76]))
                    newRow.append(somaArray(rowTest, 101-78, 77))
                    newRow.append(rowTest[101])
                    newRow.append(somaArray(rowTest, 126-103, 102))
                    newRow.append(rowTest[126])
                    newRow.append(somaArray(rowTest, 147-128, 127))
                    newRow.append(somaArray(rowTest, 167-148, 147))
                    newRow.append(somaArray(rowTest, 191-168, 167))
                    newRow.append('?')
                    allTest.append(newRow)

                writerTrain.writerows(allTrain)
                writerTest.writerows(allTest)